# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import File, InstallDir, Pybuild1

from common.nexus import NexusMod
from common.util import TRPatcher


class Package(NexusMod, TRPatcher, Pybuild1):
    NAME = "Signposts Retextured"
    DESC = "Makes the signs in Vvardenfell readable with vanilla friendly textures."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/42126"
    KEYWORDS = "openmw"
    LICENSE = "attribution-distribution"
    RDEPEND = "base/morrowind"
    TEXTURE_SIZES = "181"
    NEXUS_SRC_URI = """
        faded? (
            https://nexusmods.com/morrowind/mods/42126?tab=files&file_id=1000000241
            -> Signposts_Retextured_1-2-42126-1-2.7z
        )
        !faded? (
            https://nexusmods.com/morrowind/mods/42126?tab=files&file_id=1000000243
            -> Signposts_Retextured_2-0-42126-2-0.7z
        )
        tr? (
            faded? (
                https://nexusmods.com/morrowind/mods/42126?tab=files&file_id=1000000244
                -> Signposts_Retextured_1-2_TR-42126-1-2.7z
            )
            !faded? (
                https://nexusmods.com/morrowind/mods/42126?tab=files&file_id=1000000246
                -> Signposts_Retextured_2-0_TR-42126-2-0.7z
            )
        )
    """
    IUSE = "faded tr"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[File("PB_SignpostsRetextured.esp")],
            S="Signposts_Retextured_1-2-42126-1-2",
            REQUIRED_USE="faded",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("PB_SignpostsRetexturedTR.esp", TR_PATCH=True)],
            S="Signposts_Retextured_1-2_TR-42126-1-2",
            REQUIRED_USE="tr faded",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("PB_SignpostsRetextured.esp")],
            S="Signposts_Retextured_2-0-42126-2-0",
            REQUIRED_USE="!faded",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("PB_SignpostsRetexturedTR.esp", TR_PATCH=True)],
            S="Signposts_Retextured_2-0_TR-42126-2-0",
            REQUIRED_USE="tr !faded",
        ),
    ]
