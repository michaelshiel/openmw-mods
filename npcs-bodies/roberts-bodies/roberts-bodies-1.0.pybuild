# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import InstallDir, Pybuild1

from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    DESC = "Replaces meshes and textures of khajiit and humanoid bodies."
    HOMEPAGE = """
        https://www.nexusmods.com/morrowind/mods/43795
        https://www.nexusmods.com/morrowind/mods/48299
    """
    KEYWORDS = "openmw tes3mp"
    LICENSE = "attribution-nc all-rights-reserved"
    NAME = "Robert's Bodies"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/48299"
    IUSE = "better-clothes map_normal"
    RDEPEND = "better-clothes? ( items-clothes/better-clothes )"
    SRC_URI = """
        https://gitlab.com/portmod/mirror/-/raw/master/pluginless-roberts-bodies-0.3.7z
        better-clothes? (
            Robert's_Bodies_-_Better_Clothes_patch-48299-1-0-1591652099.7z
        )
        Robert's_Bodies_Color_Tweaks_for_Vanilla_Heads-48299-1-01-1610859961.7z
    """
    INSTALL_DIRS = [
        InstallDir(".", S="pluginless-roberts-bodies-0.3"),
        InstallDir(
            "00 Core",
            S="Robert's_Bodies_Color_Tweaks_for_Vanilla_Heads-48299-1-01-1610859961",
        ),
        InstallDir(
            "01 Normal Maps",
            S="Robert's_Bodies_Color_Tweaks_for_Vanilla_Heads-48299-1-01-1610859961",
            REQUIRED_USE="map_normal",
        ),
        InstallDir(
            "Robert's BC patch",
            S="Robert's_Bodies_-_Better_Clothes_patch-48299-1-0-1591652099",
            REQUIRED_USE="better-clothes",
        ),
    ]

    def pkg_pretend(self):
        self.warn(
            "This mod contains nude textures, but they won't show up\n"
            "in-game because the underwear meshes cover them."
        )
